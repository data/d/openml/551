# OpenML dataset: analcatdata_michiganacc

https://www.openml.org/d/551

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Jeffrey S. Simonoff  
**Source**: [StatLib](http://lib.stat.cmu.edu/datasets/) - 2003  
**Please cite**: Jeffrey S. Simonoff. Analyzing Categorical Data. Springer-Verlag, New York, 2003  

One of the data sets used in the book "Analyzing Categorical Data" by Jeffrey S. Simonoff, Springer-Verlag, New York, 2003. Further details concerning the book, including information on statistical software, are available at the [web site](http://people.stern.nyu.edu/jsimonof/AnalCatData/).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/551) of an [OpenML dataset](https://www.openml.org/d/551). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/551/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/551/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/551/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

